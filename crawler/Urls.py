class Urls:
    def __init__(self, url, domain):
        self.url = url
        self.domain = domain
        if self.domain is not None:
            try:
                self._addDomain()
            except Exception as e:
                raise Exception(self.url)
            
    def _addDomain(self):
        if self.url is not None and len(self.url) > 0:
            if self.url[0] is "/":
                if self.domain[len(self.domain) - 1] is "/":
                    self.url = self.domain + self.url
                else:
                    self.url = self.domain + '/' + self.url
            else:
                methods = ['http', 'https', 'file']
                schema = self.url.split(':')
                if len(schema) > 1:
                    if not any(s in self.url.split(':')[0] for s in methods):
                        print('not any')
                    else:
                        self.url = self.url
                else:
                    if self.url[0] == "/":
                        self.url = self.domain + self.url
                    else:
                        self.url = self.domain + "/" + self.url  
        else:
            raise Exception("not gud")


    def __str__(self):
        return self.url
