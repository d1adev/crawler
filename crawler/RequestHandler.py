import requests

class RequestHandler:
    def __init__(self):
        self.methods = {'http': self._handleHttp, 'https': self._handleHttps, 'file': self._handleFile}
        

    def request(self, url):
        if any(s in url for s in self.methods.keys()):
            schema = url.split(':')[0]
            try:
                handler = self.methods[schema]
                resp = handler(url)
                if resp is not False:
                    return resp
                else:
                    raise RequestError("Error while requesting the file")
            except KeyError:
                print("No handler found for this schema")
                raise RequestError("No request handler found for this schema")
            except RequestError as r:
                raise RequestError(r.value)

        else:
            raise ValueError("Wrong schema. http, https and file are supported")

    def _handleHttp(self, url):
        try:
            r = requests.get(url)
            if r.status_code == 200:
                if len(r.text) > 0:
                    return r.text
                else:
                    raise RequestError("The file contains less than 1 char")
            else:
                raise RequestError("Status code not 200 : " + str(r.status_code))
        except ValueError as e:
            print(e.message)
            return False
        except requests.exceptions.InvalidURL as e:
            print(e.message)
            return False
        except requests.ConnectionError as e:
            print(e)
            return False

    def _handleHttps(self, url):
        try:
            r = requests.get(url)
            if r.status_code == 200:
                if len(r.text) > 0:
                    return r.text
                else:
                    raise RequestError("The file contains less than 1 char")
            else:
                raise RequestError("Status code not 200 : " + str(r.status_code))
        except ValueError as e:
            print(e.message)
            return False
        except requests.ConnectionError as e:
            print(e.message)
            return False

    def _handleFile(self, url):
        if 'file:///' in url:
            try:
                path = url.split('file:///')[1]
                file = open(path)
                content = file.read()
                if len(content) > 0:
                    return content
                else:
                    raise RequestError("The file contains less than 1 char")
            except ValueError as e:
                print(e.message)
                raise RequestError("Error while reading the file")
        else:
            raise RequestError("Error while reading the file")
