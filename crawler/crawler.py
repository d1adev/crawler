﻿# -*- coding: utf-8 -*-
import os
from FilePolicy import FilePolicy
from CustomExceptions import RequestError
from RequestHandler import RequestHandler
from UrlParser import UrlParser
from Urls import Urls
from urllib.parse import urlparse


clear = lambda: os.system('cls') #http://stackoverflow.com/questions/517970/how-to-clear-python-interpreter-console

class Crawler:
    def __init__(self, startUrl):
        self.startUrl = startUrl
        self.urlParser = UrlParser()
        self.filePolicy = FilePolicy()
        self.requestHandler = RequestHandler()
        print("Crawler initialized")


    def Start(self):
        print('alt start')
        start = Urls(self.startUrl, None)
        urls = [start]
        visitedUrls = []
            
        while True:
            if len(urls) > 0:
                currentDomain = urls[0].url.split(':')[0] + "://" + urlparse(urls[0].url).hostname
                try:
                    clear()
                    print("Going to request " + urls[0].url + " | Visited urls : " + str(len(visitedUrls)) + " | Saved urls : " + str(len(urls)))
                except UnicodeEncodeError as e:
                    print(e)
                if self.filePolicy.isValidFile(urls[0].url) == True:
                    try:
                        resp = self.requestHandler.request(urls[0].url)
                        respUrls = self.urlParser.getUrls(resp)
                        for u in respUrls:
                            try:
                                parsedU = Urls(u, currentDomain)
                                if parsedU.url not in visitedUrls and parsedU.url not in urls:
                                    try:
                                        #print(u + ' -> ' + parsedU.url)
                                        urls.append(parsedU)
                                    except Exception as e:
                                        print('')
                            except Exception as e:
                                print('')
                    except RequestError as r:
                        print(r)
                    except ValueError as v: 
                        print(v)
                    finally: 
                        visitedUrls.append(urls[0].url)
                        urls.pop(0)
                else:
                    visitedUrls.append(urls[0].url)
                    urls.pop(0)
            else:
                print("No more urls")
                break

crawler = Crawler("https://en.wikipedia.org/wiki/Main_Page")
crawler.Start()