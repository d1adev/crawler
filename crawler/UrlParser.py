from urllib.parse import urlparse
from bs4 import BeautifulSoup

class UrlParser:
    def __init__(self):
        print("Url parser initialized")
        self.nonValidStrings = [">", "<", "'", ";"]   
    def isValidUrl(self, url, domain):
        if url is not None:
            if len(url) > 3:
                p = urlparse(url)
                if p.hostname is not None and len(p.hostname) > 3:
                    if any(s in url for s in self.nonValidStrings):
                        print(url + " isn't a valid url")
                        return False
                    else:
                        return True
                else:
                    if url[0] != "/":
                        url = "http://" + domain + "/" + url
                        return url 
                    elif url[0] == "/":
                        url = "http://" + domain + url
                        return url
                    return False

    def getUrls(self, text):
        soup = BeautifulSoup(text, 'html.parser')
        urls = []
        for link in soup.findAll('a'):
            urls.append(link.get('href'))

        return urls