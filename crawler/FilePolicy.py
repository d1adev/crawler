﻿from urllib.parse import urlparse

class FilePolicy:
    def __init__(self):
        self.acceptedExtensions = ['html', 'php', 'txt']
        print("FilePolicy initialized")

    def isValidFile(self, url):
        p = urlparse(url)
        splitedPath = p.path.split('.')
        splitedDomain = p.hostname.split('.')

        if len(splitedPath) > 0:
            if any(s == splitedPath[len(splitedPath) - 1] for s in self.acceptedExtensions):
                print(url + ' is a valid url')
                return True
            elif any('.' in s for s in splitedPath) :
                print(url + " Isn't a valid file")
                return False
            else:
                print(url + ' is a valid url')
                return True
        else:
            return True